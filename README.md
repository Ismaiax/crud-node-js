# Implementación de una solución basada en una aplicación web

Responsable del proyecto [Ismael Tello](isaiasismael.telloLI8@comunidadunir.net)

Aplicación para generar una lista de regalos, bajo un esquema CRUD en diseño MVC.


## Tabla de contenido
- [Estructura del proyecto](#estructura-del-proyecto)
- [Changelog](#changelog)

# Estructura del proyecto
> Estructura de carpetas y nombrado de archivos

## Raíz

    .
    ├── src                 # Aplicación
    ├── README.md
    └── package.json

La aplicación está realizada con NodeJS y una base de datos MongoDB. La carpeta básica contiene la carpeta del proyecto y el archivo `package.json`
Una vez clonado el proyecto se ejecuta el comando para instalar los módulos utilizados:
    `npm install`
## Proyecto
    .
    ├── ...
    ├── assets / img /favicon   # Carpeta de favicons
    ├── modelos                 # Carpeta con los archivos que manejan la base de datos
    ├── routes                  # Carpeta con los archivos que controlan las ruta
    ├── views                   # Carpeta con las vistas 
    ├── tarea.js                # Aplicación principal
    └── ...

### Assets
La carpeta de imágenes contiene los archivos necesarios para el favicon
### Modelos
La carpeta de contiene el archivo que define el esquema con las entidades que ocupa la aplicación.
| Entidad  |      Tipo     |
|----------|:--------------|
| nombre   | string        |
| regalo   | string        |
| comprado | boolean - false|
### Rutas
La carpeta contiene el archivo necesario para responder las diferentes rutas con dos métodos utilizados por la aplicación `post` y `get`.
### Vistas
Las vistas están trabajadas con el motor de plantillas EJS, con una separación básica en partes:
- Header. La parte de la cabecera del html
- Footer. El footer del sitio
- Archivos de vistas. Dos vistas principales, página de inicio y formulario de edición

### Aplicación Tarea.js
Archivo de la aplicación principal que corre en dos ambientes `dev` y `start`. El primero genera un servidor en ejecución que no se detiene con errores. El segundo genera un servidor productivo, ambos en el puerto 3000.

# Adicionales

La aplicación tiene configurada una propiedad de Univer Analytics que envía eventos a cuando se manda un formulario, cambia estado, edita o borra.

---

# Changelog
Comienzo del proyecto 28 de noviembre 2020
## [1.0.0] - 2020-11-28
### Added
- Se genera el proyecto principal con toda la estructura descrita en el presente documento