
const express = require('express');
const router = express.Router();


const Regalo = require('../models/regalos');


// Página de inicio
router.get('/', async (req, res)=>{
    const regalosGuardados = await Regalo.find();    
    res.render('index', {
        presents: regalosGuardados
    });
});


// Guardado de datos
router.post('/process', async (req, res) => {
    //console.log(new Task(req.body));
    const regalo = new Regalo(req.body);
    await regalo.save();
    res.redirect('/');
});


// Cambiar estado de comprado
router.get('/conv/:id', async (req, res)=>{
    const {id} = req.params;
    const present = await Regalo.findById(id);
    present.comprado = !present.comprado;
    await present.save();
    res.redirect('/');
});


// Borrado de datos
router.get('/delete/:id', async (req, res) => {
    const {id} = req.params;
    await Regalo.remove({_id:id});
    res.redirect('/');
});


// Editar (vista)
router.get('/edit/:id', async (req, res)=>{
    const {id} = req.params;
    const presents = await Regalo.findById(id);
    res.render('edit', {
        presents
    });
});



// Actualizar
router.post('/update/:id', async (req, res)=>{
    const {id} = req.params;
    await Regalo.update({_id:id}, req.body);
    res.redirect('/');
});


 module.exports = router;