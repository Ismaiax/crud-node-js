const express   = require('express');
const path      = require('path');
const morgan    = require('morgan');
const app       = express();
const mongoose  = require('mongoose');



// Base de datos
// Base de datos
mongoose.connect('mongodb://localhost/tarea-mongo')
    .then(db => console.log('Conectado a la base de datos'))
    .catch(err => console.log(err));



// Importar rutas
const rutas = require('./routes/rutas');


// Configuración de servidor
app.use(express.static("src"));
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


// Middleware
app.use(morgan('dev'));
app.use(express.urlencoded({
    extended : false
}));


// Rutas
app.use('/', rutas);


// Iniciando servidor
app.listen(app.get('port'), ()=> {
    console.log(`Servidor listo para correr la aplicación ${app.get('port')}`);
});