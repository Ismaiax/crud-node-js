const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const PresentsSchema = new Schema({
    nombre : String,
    regalo : String,
    comprado : {
        type : Boolean,
        default: false
    }
});

module.exports = mongoose.model('regalos', PresentsSchema);